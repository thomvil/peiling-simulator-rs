pub(crate) use crate::prelude::*;

lazy_static::lazy_static! {
    pub(crate) static ref KAMER: Assemblee = Assemblee::from_data(
        "De Kamer van Volksvertegenwoordigers",
        &[
            ("Antwerpen", 24, 56000),
            ("Oost-Vlaanderen", 20, 58000),
            ("Henegouwen", 18, 52500),
            ("West-Vlaanderen", 16, 59000),
            ("Vlaams-Brabant", 15, 55500),
            ("Brussel-Hoofdstad", 15, 41500),
            ("Luik", 15, 53000),
            ("Limburg", 12, 54000),
            ("Namen", 6, 63000),
            ("Waals-Brabant", 5, 59500),
            ("Luxemburg", 4, 18000)
        ],
    ).unwrap();

    pub(crate) static ref VP: Assemblee = Assemblee::from_data(
        "Vlaams Parlement",
        &[
            ("Antwerpen", 33, 40000),
            ("Oost-Vlaanderen", 27, 42000),
            ("West-Vlaanderen", 22, 42000),
            ("Vlaams-Brabant", 20, 41000),
            ("Brussel-Hoofdstad", 6, 13000),
            ("Limburg", 16, 40000),
        ],
    ).unwrap();

    pub(crate) static ref BHP: Assemblee = Assemblee::from_data(
        "Brussels Hoofdstedelijk Parlement",
        &[
            ("Nederlandstalig Kiescollege", 17, 5000),
            ("Franstalig Kiescollege", 72, 6000),
        ],
    ).unwrap();

    pub(crate) static ref VP_2019_DATA: &'static [(&'static str, &'static [(&'static str, u32)])] = &[
        ("West-Vlaanderen", &[
            ("Open Vld", 98678),
            ("N-VA", 157198),
            ("VLAAMS BELANG", 161586),
            ("CD&V", 187637),
            ("PVDA", 27393),
            ("GROEN", 56558),
            ("sp.a", 97398),
            ("DierAnimal", 8745),
            ("Piratenpartij", 4161)]),
        ("Limburg", &[
            ("Open Vld", 65086),
            ("N-VA", 120633),
            ("VLAAMS BELANG", 110351),
            ("CD&V", 105133),
            ("PVDA", 31272),
            ("GROEN", 36762),
            ("sp.a" ,74161),
            ("PRO", 1445),
            ("D-SA", 2036),
            ("RP", 705)
        ]),
        ("Vlaams-Brabant", &[
            ("Open Vld", 109634),
            ("N-VA", 181032),
            ("VLAAMS BELANG", 93118),
            ("CD&V", 91619),
            ("PVDA", 32366),
            ("UF", 28804),
            ("GROEN", 85852),
            ("sp.a", 66701),
            ("DierAnimal", 8978),
            ("PRO", 4240)
        ]),
        ("Antwerpen", &[
            ("Open Vld", 116020),
            ("N-VA", 361403),
            ("VLAAMS BELANG", 209738),
            ("CD&V", 129843),
            ("PVDA", 75833),
            ("GROEN", 126988),
            ("sp.a", 90113),
            ("DierAnimal", 9414),
            ("PV&S", 2813),
            ("D-SA", 2533),
            ("Piratenpartij", 4987),
            ("Genoeg vr iedereen", 2650),
            ("BE.ONE", 1563),
            ("Burgerlijst", 2033)
        ]),
        ("Oost-Vlaanderen", &[
            ("Open Vld", 156275),
            ("N-VA", 219289),
            ("VLAAMS BELANG", 203497),
            ("CD&V", 132894),
            ("PVDA", 54457),
            ("GROEN", 103764),
            ("sp.a", 101258),
            ("DierAnimal", 9807),
            ("de coöperatie", 1402),
            ("RP", 954),
            ("Be.One", 2501)
        ]),
        ("Brussel-Hoofdstad", &[
            ("Open Vld", 10937),
            ("N-VA", 12697),
            ("VLAAMS BELANG", 5687),
            ("CD&V", 5640),
            ("PVDA", 4272),
            ("GROEN", 18772),
            ("sp.a-one brussels", 8958)
        ]),
    ];

    pub(crate) static ref VP_2019: VerkiezingsResultaat<'static> = VerkiezingsResultaat::from_data(
        &VP,
        0.05,
        &VP_2019_DATA
    ).unwrap();

    pub(crate) static ref VLAAMS_NATIONALISTISCH: Coalitie = Coalitie::from_data("Vlaams-Nationalistisch", &["Vlaams Belang", "N-VA"]).unwrap();
    pub(crate) static ref ZWEEDS: Coalitie = Coalitie::from_data("Zweeds", &["N-VA", "OpenVLD", "CD&V"]).unwrap();
    pub(crate) static ref PAARS_GROEN: Coalitie = Coalitie::from_data("Paars-Groen", &["OpenVLD", "Vooruit", "Groen"]).unwrap();
    pub(crate) static ref PAARS_GEEL: Coalitie = Coalitie::from_data("Paars-Geel", &["OpenVLD", "Vooruit", "N-VA"]).unwrap();
    pub(crate) static ref KLASSIEKE_TRIPARTITE: Coalitie = Coalitie::from_data("Klassieke Tripartite", &["CD&V", "OpenVLD", "Vooruit"]).unwrap();
    pub(crate) static ref VIVALDI: Coalitie = Coalitie::from_data("Vivaldi", &["OpenVLD", "Vooruit", "Groen", "CD&V"]).unwrap();
    pub(crate) static ref ANTI_VLAAMS_NATIONALISTISCH: Coalitie = Coalitie::from_data("Anti-Vlaams-Nationalistisch", &["OpenVLD", "Vooruit", "Groen", "CD&V", "Pvda"]).unwrap();
    pub(crate) static ref VIER_SEIZOENEN: Coalitie = Coalitie::from_data("N-VA + Cd&V + OpenVLD + Vooruit", &["N-VA", "CD&V", "OpenVLD", "Vooruit"]).unwrap();

    pub(crate) static ref COALITIES: [&'static Coalitie; 8] = [
        &VLAAMS_NATIONALISTISCH,
        &ANTI_VLAAMS_NATIONALISTISCH,
        &ZWEEDS,
        &PAARS_GROEN,
        &PAARS_GEEL,
        &KLASSIEKE_TRIPARTITE,
        &VIVALDI,
        &VIER_SEIZOENEN
    ];

    pub(crate) static ref PEILING_MEI_2021: &'static[(&'static str, f32)] = &[
        ("CD&V", 10.),
        ("Groen", 10.9),
        ("N-VA", 21.5),
        ("OpenVLD", 11.5),
        ("PVDA", 7.9),
        ("Vooruit", 12.),
        ("Vlaams Belang", 24.7),
        ("-Andere-", 1.4),
    ];
}
