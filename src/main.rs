#![allow(unused_imports, dead_code)]

mod calculations;
mod cli;
mod data;
mod model;
mod prelude;

pub(crate) use prelude::*;

fn main() {
    let peiling = Peiling::from_data(&VP, 5., 2.5, &PEILING_MEI_2021).unwrap();
    let nb_simulaties = 100_000;
    let mut ps = PeilingSimulator::from_peiling(peiling);
    ps.run(nb_simulaties);
    ps.zetels_report();
    ps.coalitie_report();
    ps.coalitie_report_extended();

    // let voorstel_brusselse_peiling = [
    //     ("Vlaams Belang", 16956, 19.5),
    //     ("N-VA", 22455, 25.8),
    //     ("CD&V", 6874, 7.9),
    //     ("OpenVLD", 12437, 14.3),
    //     ("one.brussels", 9503, 10.9),
    //     ("Groen", 15581, 17.9),
    //     ("PVDA", 3125, 3.6),
    // ];
}
