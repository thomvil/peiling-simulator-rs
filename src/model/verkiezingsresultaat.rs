pub(crate) use crate::prelude::*;

#[derive(Debug, Clone)]
pub(crate) struct VerkiezingsResultaat<'a> {
    assemblee: &'a Assemblee,
    kiesdrempel: f32,
    resultaten: Vec<(KieskringNaam, StemmenAantal)>,
}

impl<'a> VerkiezingsResultaat<'a> {
    pub(crate) fn new(
        assemblee: &'a Assemblee,
        kiesdrempel: f32,
        resultaten: Vec<(KieskringNaam, StemmenAantal)>,
    ) -> Self {
        Self {
            assemblee,
            kiesdrempel,
            resultaten,
        }
    }

    pub(crate) fn from_data(
        assemblee: &'a Assemblee,
        kiesdrempel: f32,
        resultaten_data: &[(&str, &[(&str, u32)])],
    ) -> Result<Self, String> {
        let mut resultaten = Vec::new();
        for (kieskring_naam, stemmen_lijst) in resultaten_data {
            let kieskring = KieskringNaam::from_str(&kieskring_naam)?;
            let stemmen_aantal = StemmenAantal::from_data(&stemmen_lijst)?;
            resultaten.push((kieskring, stemmen_aantal));
        }
        Ok(Self::new(assemblee, kiesdrempel, resultaten))
    }

    // TODO: verwijderen
    pub(crate) fn zetel_verdeling_per_kieskring(
        &self,
    ) -> Vec<(KieskringNaam, HashMap<Partij, u32>)> {
        self.resultaten
            .iter()
            .map(|(kieskring, stemmen_lijst)| {
                let partij_zetels = DhondtZetelVerdeler::zetel_verdeling(
                    self.assemblee.zetels(*kieskring),
                    self.kiesdrempel,
                    stemmen_lijst,
                );
                (*kieskring, partij_zetels)
            })
            .collect()
    }

    pub(crate) fn zetel_verdeling_per_kieskring2(&self) -> HashMap<(KieskringNaam, Partij), u32> {
        self.resultaten
            .iter()
            .fold(HashMap::new(), |mut acc, (kieskring, stemmen_aantal)| {
                let partij_zetels = DhondtZetelVerdeler::zetel_verdeling(
                    self.assemblee.zetels(*kieskring),
                    self.kiesdrempel,
                    stemmen_aantal,
                );
                for (partij, zetels) in partij_zetels {
                    acc.insert((*kieskring, partij), zetels);
                }
                acc
            })
    }
}

#[derive(Debug, Clone)]
pub(crate) struct StemmenAantal(Vec<(Partij, u32)>);

impl StemmenAantal {
    pub(crate) fn new(list: Vec<(Partij, u32)>) -> Self {
        Self(list)
    }

    pub(crate) fn iter(&self) -> impl Iterator<Item = (Partij, u32)> + '_ {
        self.0.iter().map(|(p, s)| (*p, *s))
    }

    pub(crate) fn from_data(list: &[(&str, u32)]) -> Result<Self, String> {
        list.iter()
            .map(|(partij_naam, stemmen)| Partij::from_str(&partij_naam).map(|p| (p, *stemmen)))
            .collect::<Result<Vec<_>, _>>()
            .map(Self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn vp_2019() {
        // let zetels = VP_2019.zetel_verdeling();
        // assert_eq!(23, *zetels.get(&Partij::VlaamsBelang).unwrap());
        // assert_eq!(35, *zetels.get(&Partij::Nva).unwrap());
        // assert_eq!(19, *zetels.get(&Partij::Cdenv).unwrap());
        // assert_eq!(16, *zetels.get(&Partij::OpenVld).unwrap());
        // assert_eq!(13, *zetels.get(&Partij::Vooruit).unwrap());
        // assert_eq!(14, *zetels.get(&Partij::Groen).unwrap());
        // assert_eq!(4, *zetels.get(&Partij::Pvda).unwrap());
        // assert_eq!(0, *zetels.get(&Partij::Uf).unwrap());
    }
}
