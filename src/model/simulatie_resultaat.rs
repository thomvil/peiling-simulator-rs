use crate::prelude::*;

#[derive(Debug, Default)]
pub(crate) struct SimulatieResultaat {
    lijst: HashMap<(KieskringNaam, Partij), Vec<u32>>,
    totalen: HashMap<Partij, Vec<u32>>,
    partijen: HashSet<Partij>,
    kieskringen: HashSet<KieskringNaam>,
}

impl SimulatieResultaat {
    pub(crate) fn add(&mut self, sim: HashMap<(KieskringNaam, Partij), u32>) {
        let totalen = sim.iter().fold(
            HashMap::new(),
            |mut acc: HashMap<Partij, u32>, ((_k, p), z)| {
                *acc.entry(*p).or_insert(0) += z;
                acc
            },
        );

        let current_lijst_keys = self.lijst.keys().copied().collect::<HashSet<_>>();
        let current_totaal_keys = self.totalen.keys().copied().collect::<HashSet<_>>();

        let added_lijst_keys = sim.keys().copied().collect::<HashSet<_>>();
        let added_totaal_keys = totalen.keys().copied().collect::<HashSet<_>>();

        let missing_lijst_keys = current_lijst_keys
            .difference(&added_lijst_keys)
            .copied()
            .collect::<HashSet<_>>();
        let missing_totaal_keys = current_totaal_keys
            .difference(&added_totaal_keys)
            .copied()
            .collect::<HashSet<_>>();

        let new_lijst_keys = added_lijst_keys
            .difference(&current_lijst_keys)
            .copied()
            .collect::<HashSet<_>>();
        let new_totaal_keys = added_totaal_keys
            .difference(&current_totaal_keys)
            .copied()
            .collect::<HashSet<_>>();

        let nb_sim = self.nb_simulaties();
        for key in new_lijst_keys {
            self.lijst.insert(key, vec![0; nb_sim]);
        }
        for key in new_totaal_keys {
            self.totalen.insert(key, vec![0; nb_sim]);
        }

        for ((k, p), z) in sim {
            self.lijst.entry((k, p)).or_insert_with(Vec::new).push(z);
            self.partijen.insert(p);
            self.kieskringen.insert(k);
        }
        for (p, t) in totalen {
            self.totalen.entry(p).or_insert_with(Vec::new).push(t);
        }
        for missing_key in missing_lijst_keys {
            self.lijst.get_mut(&missing_key).unwrap().push(0);
        }
        for missing_key in missing_totaal_keys {
            self.totalen.get_mut(&missing_key).unwrap().push(0);
        }
    }

    pub(crate) fn nb_simulaties(&self) -> usize {
        self.lijst
            .values()
            .next()
            .map(|sim_lijst| sim_lijst.len())
            .unwrap_or(0)
    }

    pub(crate) fn kieskringen(&self) -> impl Iterator<Item = KieskringNaam> + '_ {
        self.kieskringen.iter().copied()
    }

    pub(crate) fn get(&self, key: &(KieskringNaam, Partij)) -> Option<&Vec<u32>> {
        self.lijst.get(key)
    }

    pub(crate) fn get_totaal(&self, key: &Partij) -> Option<&Vec<u32>> {
        self.totalen.get(key)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add() {
        use KieskringNaam::*;
        use Partij::*;

        let mut sr = SimulatieResultaat::default();

        let mut sim = HashMap::new();
        sim.insert((KieskringNaam::WestVlaanderen, Partij::Nva), 10);
        sim.insert((KieskringNaam::WestVlaanderen, Partij::OpenVld), 5);
        sim.insert((KieskringNaam::Antwerpen, Partij::OpenVld), 1);

        sr.add(sim);

        assert_eq!(
            &vec![10],
            sr.get(&(KieskringNaam::WestVlaanderen, Partij::Nva))
                .unwrap()
        );
        assert_eq!(&vec![6], sr.get_totaal(&Partij::OpenVld).unwrap());

        let mut sim = HashMap::new();
        sim.insert((KieskringNaam::WestVlaanderen, Partij::Nva), 10);
        sim.insert((KieskringNaam::Limburg, Partij::OpenVld), 5);
        sim.insert((KieskringNaam::Antwerpen, Partij::Vooruit), 1);

        sr.add(sim);

        assert_eq!(&vec![10, 10], sr.get(&(WestVlaanderen, Nva)).unwrap());
        assert_eq!(&vec![5, 0], sr.get(&(WestVlaanderen, OpenVld)).unwrap());
        assert_eq!(&vec![1, 0], sr.get(&(Antwerpen, OpenVld)).unwrap());
        assert_eq!(&vec![0, 5], sr.get(&(Limburg, OpenVld)).unwrap());
        assert_eq!(&vec![0, 1], sr.get(&(Antwerpen, Vooruit)).unwrap());
    }
}
