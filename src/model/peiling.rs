pub(crate) use crate::prelude::*;

#[derive(Debug, Clone)]
pub(crate) struct Peiling<'a> {
    pub(crate) assemblee: &'a Assemblee,
    kiesdrempel: f32,
    foutenmarge: f32,
    resultaten: Vec<(Partij, f32)>,
}

impl<'a> Peiling<'a> {
    pub(crate) fn from_data(
        assemblee: &'a Assemblee,
        kiesdrempel: f32,
        foutenmarge: f32,
        resultaten_data: &[(&str, f32)],
    ) -> Result<Self, String> {
        let mut resultaten = Vec::new();
        for (partij_naam, perc) in resultaten_data {
            let partij = Partij::from_str(partij_naam)?;
            resultaten.push((partij, *perc));
        }
        Ok(Self {
            assemblee,
            kiesdrempel,
            foutenmarge,
            resultaten,
        })
    }

    // pub(crate) fn iter(&self) -> impl Iterator<Item = (Partij, f32)> + '_ {
    //     self.resultaten.iter().copied()
    // }

    // pub(crate) fn simulate_zetelverdeling(&self) -> HashMap<Partij, u32> {
    //     let sim_res: Vec<(KieskringNaam, StemmenAantal)> = self
    //         .assemblee
    //         .kieskringen()
    //         .map(|kkn| {
    //             let stemmen_lijst = generate_variation(self.foutenmarge, &self.resultaten)
    //                 .iter()
    //                 .map(|(partij, perc)| {
    //                     let stemmen = *perc / 100. * (self.assemblee.kiezers(kkn) as f32);
    //                     (*partij, stemmen as u32)
    //                 })
    //                 .collect();
    //             (kkn, StemmenAantal::new(stemmen_lijst))
    //         })
    //         .collect();

    //     VerkiezingsResultaat::new(self.assemblee, self.kiesdrempel, sim_res).zetel_verdeling()
    // }

    pub(crate) fn simulate_zetelverdeling_per_kieskring(
        &self,
    ) -> Vec<(KieskringNaam, HashMap<Partij, u32>)> {
        let sim_res: Vec<(KieskringNaam, StemmenAantal)> = self
            .assemblee
            .kieskringen()
            .map(|kkn| {
                let stemmen_lijst = generate_variation(self.foutenmarge, &self.resultaten)
                    .iter()
                    .map(|(partij, perc)| {
                        let stemmen = *perc / 100. * (self.assemblee.kiezers(kkn) as f32);
                        (*partij, stemmen as u32)
                    })
                    .collect();
                (kkn, StemmenAantal::new(stemmen_lijst))
            })
            .collect();

        VerkiezingsResultaat::new(self.assemblee, self.kiesdrempel, sim_res)
            .zetel_verdeling_per_kieskring()
    }

    // pub(crate) fn simulate_zetelverdeling_constante_andere(&self) -> HashMap<Partij, u32> {
    //     let sim_res: Vec<(KieskringNaam, StemmenAantal)> = self
    //         .assemblee
    //         .kieskringen()
    //         .map(|kkn| {
    //             let stemmen_lijst =
    //                 generate_variation_except_last(self.foutenmarge, &self.resultaten)
    //                     .iter()
    //                     .map(|(partij, perc)| {
    //                         let stemmen = *perc / 100. * (self.assemblee.kiezers(kkn) as f32);
    //                         (*partij, stemmen as u32)
    //                     })
    //                     .collect();
    //             (kkn, StemmenAantal::new(stemmen_lijst))
    //         })
    //         .collect();

    //     VerkiezingsResultaat::new(self.assemblee, self.kiesdrempel, sim_res).zetel_verdeling()
    // }

    pub(crate) fn simulate_zetelverdeling_per_kieskring_constante_andere(
        &self,
    ) -> Vec<(KieskringNaam, HashMap<Partij, u32>)> {
        let sim_res: Vec<(KieskringNaam, StemmenAantal)> = self
            .assemblee
            .kieskringen()
            .map(|kkn| {
                let stemmen_lijst =
                    generate_variation_except_last(self.foutenmarge, &self.resultaten)
                        .iter()
                        .map(|(partij, perc)| {
                            let stemmen = *perc / 100. * (self.assemblee.kiezers(kkn) as f32);
                            (*partij, stemmen as u32)
                        })
                        .collect();
                (kkn, StemmenAantal::new(stemmen_lijst))
            })
            .collect();

        VerkiezingsResultaat::new(self.assemblee, self.kiesdrempel, sim_res)
            .zetel_verdeling_per_kieskring()
    }
}
