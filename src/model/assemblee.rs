pub(crate) use crate::prelude::*;

#[derive(Debug, Clone)]
pub(crate) struct Assemblee {
    pub(crate) naam: String,
    kieskringen: HashMap<KieskringNaam, Kieskring>,
}

impl Assemblee {
    pub(crate) fn new(
        naam: &str,
        kieskringen: impl Iterator<Item = (KieskringNaam, Kieskring)>,
    ) -> Self {
        Self {
            naam: naam.to_string(),
            kieskringen: kieskringen.collect(),
        }
    }

    pub(crate) fn from_data(
        naam: &str,
        kieskring_list: &[(&str, u32, u32)],
    ) -> Result<Self, String> {
        kieskring_list
            .iter()
            .map(|(naam_str, zetels, kiezers_per_zetel)| {
                Kieskring::from_data(naam_str, *zetels, *kiezers_per_zetel)
            })
            .collect::<Result<HashMap<_, _>, _>>()
            .map(|kieskringen| Self {
                naam: naam.to_string(),
                kieskringen,
            })
    }

    pub(crate) fn totale_zetels(&self) -> u32 {
        self.kieskringen
            .values()
            .fold(0, |acc, kieskring| acc + kieskring.zetels)
    }

    pub(crate) fn kieskringen(&self) -> impl Iterator<Item = KieskringNaam> + '_ {
        self.kieskringen.keys().copied()
    }

    pub(crate) fn zetels(&self, kieskring: KieskringNaam) -> u32 {
        self.kieskringen
            .get(&kieskring)
            .copied()
            .map(|kk| kk.zetels)
            .expect("Kieskring niet gevonden in deze assemblee")
    }

    pub(crate) fn kiezers(&self, kieskring: KieskringNaam) -> u32 {
        self.kieskringen
            .get(&kieskring)
            .copied()
            .map(|kk| kk.kiezers())
            .expect("Kieskring niet gevonden in deze assemblee")
    }

    pub(crate) fn iter(&self) -> impl Iterator<Item = (KieskringNaam, Kieskring)> + '_ {
        self.kieskringen.iter().map(|(k, v)| (*k, *v))
    }
}
