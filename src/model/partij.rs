pub(crate) use crate::prelude::*;

#[allow(clippy::enum_variant_names)]
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub(crate) enum Partij {
    VlaamsBelang,
    Nva,
    Cdenv,
    OpenVld,
    Vooruit,
    Groen,
    Pvda,
    Uf,
    DierAnimal,
    PiratenPartij,
    BeOne,
    Burgerlijst,
    DSa,
    DeCooperatie,
    GenoegVoorIedereen,
    Pro,
    PVenS,
    Rp,
    Volt,
    Andere,
}

impl FromStr for Partij {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let reduced_s = s
            .chars()
            .filter(|c| c.is_ascii_alphanumeric())
            .map(|c| c.to_ascii_lowercase())
            .collect::<String>();

        match reduced_s.as_str() {
            "vlaamsbelang" | "vlaamsblok" => Ok(Self::VlaamsBelang),
            "nva" | "vu" | "volksunie" => Ok(Self::Nva),
            "openvld" | "vld" | "pvv" => Ok(Self::OpenVld),
            "cdv" | "cvp" => Ok(Self::Cdenv),
            "vooruit" | "spa" | "sp" | "spaonebrussels" | "onebrussels" => Ok(Self::Vooruit),
            "groen" | "agalev" => Ok(Self::Groen),
            "pvda" | "ptb" | "pvdaptb" | "ptbpvda" | "amada" => Ok(Self::Pvda),
            "uf" | "uniondesfrancophones" | "unionfrancophones" => Ok(Self::Uf),
            "dieranimal" => Ok(Self::DierAnimal),
            "piratenpartij" => Ok(Self::PiratenPartij),
            "pro" => Ok(Self::Pro),
            "dsa" => Ok(Self::DSa),
            "rp" => Ok(Self::Rp),
            "pvs" => Ok(Self::PVenS),
            "genoegvriedereen" | "genoegvooriedereen" => Ok(Self::GenoegVoorIedereen),
            "beone" => Ok(Self::BeOne),
            "burgerlijst" => Ok(Self::Burgerlijst),
            "decoperatie" | "coperatie" | "decooperatie" | "cooperatie" => Ok(Self::DeCooperatie),
            "volt" => Ok(Self::Volt),
            "andere" => Ok(Self::Andere),
            _ => Err(format!("'{}' niet herkend als partijnaam", s)),
        }
    }
}

impl Display for Partij {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::VlaamsBelang => "Vlaams Belang",
            Self::Nva => "N-VA",
            Self::Cdenv => "CD&V",
            Self::OpenVld => "OpenVLD",
            Self::Vooruit => "Vooruit",
            Self::Groen => "Groen",
            Self::Pvda => "PVDA",
            Self::Uf => "UF",
            Self::DierAnimal => "DierAnimal",
            Self::PiratenPartij => "PiratenPartij",
            Self::BeOne => "Be.One",
            Self::Burgerlijst => "Burgerlijst",
            Self::DSa => "D-SA",
            Self::DeCooperatie => "De Coöperatie",
            Self::GenoegVoorIedereen => "Genoeg Voor Iedereen",
            Self::Pro => "Pro",
            Self::PVenS => "PV&S",
            Self::Rp => "RP",
            Self::Volt => "Volt",
            Self::Andere => "-Andere-",
        };
        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse() {
        assert_eq!(Ok(Partij::OpenVld), Partij::from_str("Open-Vld"));
    }
}
