pub(crate) use crate::prelude::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub(crate) struct Kieskring {
    pub(crate) naam: KieskringNaam,
    pub(crate) zetels: u32,
    pub(crate) kiezers_per_zetel: u32,
}

impl Kieskring {
    pub(crate) fn new(naam: KieskringNaam, zetels: u32, kiezers_per_zetel: u32) -> Self {
        Self {
            naam,
            zetels,
            kiezers_per_zetel,
        }
    }

    pub(crate) fn from_data(
        naam_str: &str,
        zetels: u32,
        kiezers_per_zetel: u32,
    ) -> Result<(KieskringNaam, Self), String> {
        let naam = KieskringNaam::from_str(naam_str)?;
        Ok((naam, Self::new(naam, zetels, kiezers_per_zetel)))
    }

    pub(crate) fn kiezers(&self) -> u32 {
        self.zetels * self.kiezers_per_zetel
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub(crate) enum KieskringNaam {
    WestVlaanderen,
    OostVlaanderen,
    Antwerpen,
    VlaamsBrabant,
    Limburg,
    BrusselHoofdstad,
    Henegouwen,
    Luik,
    WaalsBrabant,
    Namen,
    Luxemburg,
}

impl FromStr for KieskringNaam {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let reduced_s = s
            .chars()
            .filter(|c| c.is_ascii_alphanumeric())
            .map(|c| c.to_ascii_lowercase())
            .collect::<String>();

        match reduced_s.as_str() {
            "westvlaanderen" | "wvl" => Ok(Self::WestVlaanderen),
            "oostvlaanderen" | "ovl" => Ok(Self::OostVlaanderen),
            "vlaamsbrabant" | "vb" | "vlbr" => Ok(Self::VlaamsBrabant),
            "limburg" | "limb" | "lim" => Ok(Self::Limburg),
            "antwerpen" | "apen" | "antw" | "ant" => Ok(Self::Antwerpen),
            "brusselhoofdstad"
            | "brussel"
            | "brusselhoofdstedelijkgewest"
            | "brusselsgewest"
            | "bxl"
            | "bsl"
            | "bruxelles"
            | "bruxellescapitale"
            | "brux" => Ok(Self::BrusselHoofdstad),
            "henegouwen" | "hainaut" | "hene" | "hai" => Ok(Self::Henegouwen),
            "luik" | "lige" | "liege" | "li" | "lie" => Ok(Self::Luik),
            "waalsbrabant" | "wabr" | "wb" | "brabantwallon" | "brwa" | "bw" => {
                Ok(Self::WaalsBrabant)
            }
            "namen" | "namur" | "nam" => Ok(Self::Namen),
            "luxemburg" | "luxembourg" | "lux" => Ok(Self::Luxemburg),
            _ => Err(format!("'{}' niet herkend als kieskring", s)),
        }
    }
}

impl Display for KieskringNaam {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::WestVlaanderen => "West-Vlaanderen",
            Self::OostVlaanderen => "Oost-Vlaanderen",
            Self::Antwerpen => "Antwerpen",
            Self::VlaamsBrabant => "Vlaams-Brabant",
            Self::Limburg => "Limburg",
            Self::BrusselHoofdstad => "Brussel-Hoofdstad",
            Self::Henegouwen => "Henegouwen",
            Self::Luik => "Luik",
            Self::WaalsBrabant => "Waals-Brabant",
            Self::Namen => "Namen",
            Self::Luxemburg => "Luxemburg",
        };
        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse() {
        assert_eq!(
            Ok(KieskringNaam::BrusselHoofdstad),
            KieskringNaam::from_str("bxl")
        );
    }
}
