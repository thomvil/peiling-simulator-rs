use crate::prelude::*;

#[derive(Debug, Clone)]
pub(crate) struct Coalitie {
    pub(crate) naam: String,
    partijen: HashSet<Partij>,
}

impl Coalitie {
    pub(crate) fn from_data(naam: &str, namen: &[&str]) -> Result<Self, String> {
        namen
            .iter()
            .map(|partij_naam| Partij::from_str(partij_naam))
            .collect::<Result<HashSet<_>, _>>()
            .map(|partijen| Self {
                naam: naam.to_string(),
                partijen,
            })
    }

    pub(crate) fn iter(&self) -> impl Iterator<Item = Partij> + '_ {
        self.partijen.iter().copied()
    }
}
