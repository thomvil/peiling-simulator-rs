use crate::prelude::*;

pub(crate) type Summary = (u32, u32, u32, u32, u32);

pub(crate) fn stats(list: &[u32]) -> Summary {
    let mut tmp = list.iter().copied().collect::<Vec<_>>();
    tmp.sort_unstable();
    let l = tmp.len();
    let min = tmp[0];
    let perc5 = tmp[(l as f32 * 0.05) as usize];
    let perc50 = tmp[(l as f32 * 0.5) as usize];
    let perc95 = tmp[(l as f32 * 0.95) as usize];
    let max = tmp[l - 1];

    (min, perc5, perc50, perc95, max)
}

pub(crate) fn perc_exceeding(list: &[u32], value: u32) -> f32 {
    let l = list.len() as f32;
    list.iter().filter(|e| **e >= value).count() as f32 / l * 100.
}

fn pos_neg_sum(list: &[f32]) -> (f32, f32) {
    list.iter()
        .copied()
        .fold((0., 0.), |(acc_pos_sum, acc_neg_sum), e| {
            if e < 0. {
                (acc_pos_sum, acc_neg_sum + e)
            } else {
                (acc_pos_sum + e, acc_neg_sum)
            }
        })
}

pub(crate) fn generate_diff(foutenmarge: f32, length: usize) -> Vec<f32> {
    let mut rng = thread_rng();
    let foutenmarge_dist = Uniform::new_inclusive(-10000., 10000.);
    let var_dist = Uniform::new_inclusive(0.1, foutenmarge.abs());

    let mut res = foutenmarge_dist
        .sample_iter(&mut rng)
        .take(length)
        .map(|e: f32| if e.abs() < 0.0001 { 1. } else { e })
        .collect::<Vec<f32>>();
    if res.iter().all(|e| *e > 0.) || res.iter().all(|e| *e < 0.) {
        res[0] *= -1.;
        res[length - 1] *= -1.;
    }
    if res.iter().all(|e| e.abs() < 1.) {
        res[0] = 11.;
        res[length - 1] = -19.;
    }

    let (pos_sum, neg_sum) = pos_neg_sum(&res);

    res.iter_mut().for_each(|e| {
        if *e < 0. {
            *e /= neg_sum.abs()
        } else {
            *e /= pos_sum
        }
    });
    let max_abs = res
        .iter()
        .copied()
        .max_by_key(|e| (e.abs() * 100000.) as u32)
        .unwrap();

    let delta: f32 = var_dist.sample(&mut rng);
    res.iter_mut().for_each(|e| *e = *e / max_abs.abs() * delta);
    res
}

pub(crate) fn generate_variation(foutenmarge: f32, list: &[(Partij, f32)]) -> Vec<(Partij, f32)> {
    let mut res = generate_variation_raw(foutenmarge, list);
    while res.iter().any(|(_partij, perc)| *perc <= 0.) {
        res = generate_variation_raw(foutenmarge, list);
    }
    res
}

pub(crate) fn generate_variation_except_last(
    foutenmarge: f32,
    list: &[(Partij, f32)],
) -> Vec<(Partij, f32)> {
    let mut res = generate_variation_except_last_raw(foutenmarge, list);
    while res.iter().any(|(_partij, perc)| *perc <= 0.) {
        res = generate_variation_except_last_raw(foutenmarge, list);
    }
    res
}

fn generate_variation_raw(foutenmarge: f32, list: &[(Partij, f32)]) -> Vec<(Partij, f32)> {
    list.iter()
        .zip(generate_diff(foutenmarge, list.len()))
        .map(|((partij, perc), diff)| (*partij, *perc + diff))
        .collect()
}

fn generate_variation_except_last_raw(
    foutenmarge: f32,
    list: &[(Partij, f32)],
) -> Vec<(Partij, f32)> {
    list.iter()
        .zip(
            generate_diff(foutenmarge, list.len() - 1)
                .into_iter()
                .chain(once(0.)),
        )
        .map(|((partij, perc), diff)| (*partij, *perc + diff))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const NB_TESTS: u32 = 100_000;

    #[test]
    fn generate_variation_sum_and_max() {
        let length = 7;
        let foutenmarge = 2.4;

        (0..NB_TESTS)
            .into_par_iter()
            .map(|_| {
                let variation = generate_diff(foutenmarge, length);
                let max_abs = variation
                    .iter()
                    .copied()
                    .max_by_key(|e| (e.abs() * 100000.) as u32)
                    .unwrap()
                    .abs();
                let sum = variation.iter().copied().sum::<f32>();
                assert!(max_abs <= foutenmarge);
                assert!(sum < 0.0001);
            })
            .count();
    }

    #[test]
    fn generate_variation_non_zero() {
        let foutenmarge = 2.4;

        (0..NB_TESTS)
            .into_par_iter()
            .map(|_| {
                let tmp = generate_variation(foutenmarge, &peiling());
                assert!(tmp.iter().all(|(_, perc)| *perc > 0.));
            })
            .count();
    }

    #[test]
    fn generate_variation_except_last_non_zero() {
        let foutenmarge = 2.4;
        let andere_perc = peiling().last().unwrap().1;

        (0..NB_TESTS)
            .into_par_iter()
            .map(|_| {
                let tmp = generate_variation_except_last(foutenmarge, &peiling());
                assert!(tmp.iter().all(|(_, perc)| *perc > 0.));
                assert!((andere_perc - tmp.last().unwrap().1).abs() < 0.0001);
            })
            .count();
    }

    fn peiling() -> Vec<(Partij, f32)> {
        [
            ("CD&V", 0.1),
            ("Groen", 0.109),
            ("N-VA", 0.215),
            ("OpenVLD", 0.115),
            ("PVDA", 0.079),
            ("Vooruit", 0.12),
            ("Vlaams Belang", 0.247),
            ("-Andere-", 0.014),
        ]
        .iter()
        .map(|(partij_naam, perc)| (Partij::from_str(partij_naam).unwrap(), *perc))
        .collect()
    }
}
