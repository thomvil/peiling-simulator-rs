pub(crate) use crate::prelude::*;

pub(crate) struct DhondtZetelVerdeler;

impl DhondtZetelVerdeler {
    fn totaal_stemmen(resultaten: &StemmenAantal) -> f32 {
        resultaten.iter().map(|(_, stemmen)| stemmen as f32).sum()
    }

    fn kiesdelers(
        nb_zetels: u32,
        kiesdrempel: f32,
        resultaten: &StemmenAantal,
    ) -> Vec<(Partij, u32, u32)> {
        let totaal_stemmen = Self::totaal_stemmen(resultaten);
        resultaten
            .iter()
            .filter(|(_partij, stemmen)| *stemmen as f32 / totaal_stemmen >= (kiesdrempel / 100.))
            .map(|(partij, stemmen)| {
                (1..=nb_zetels)
                    .map(|deler| (partij, stemmen * 1000 / deler, stemmen))
                    .collect::<Vec<_>>()
            })
            .flatten()
            .collect()
    }

    fn partij_hm(resultaten: &StemmenAantal) -> HashMap<Partij, u32> {
        resultaten
            .iter()
            .fold(HashMap::new(), |mut acc, (partij, _)| {
                acc.entry(partij).or_insert(0);
                acc
            })
    }

    pub(crate) fn zetel_verdeling(
        nb_zetels: u32,
        kiesdrempel: f32,
        resultaten: &StemmenAantal,
    ) -> HashMap<Partij, u32> {
        let mut delers = Self::kiesdelers(nb_zetels, kiesdrempel, resultaten);
        delers.sort_unstable_by_key(|(_partij, deler, stemmen)| (*deler, *stemmen));
        delers.into_iter().rev().take(nb_zetels as usize).fold(
            Self::partij_hm(resultaten),
            |mut acc, (partij, _deler, _stemmen)| {
                *acc.get_mut(&partij).unwrap() += 1;
                acc
            },
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn wikipedia() {
        let nb_zetels = 25;
        let resultaten =
            StemmenAantal::from_data(&[("N-VA", 6500), ("Open vld", 3800), ("sp.a", 2300)])
                .unwrap();

        let zetels = DhondtZetelVerdeler::zetel_verdeling(nb_zetels, 0., &resultaten);
        assert_eq!(13, *zetels.get(&Partij::Nva).unwrap());
        assert_eq!(8, *zetels.get(&Partij::OpenVld).unwrap());
        assert_eq!(4, *zetels.get(&Partij::Vooruit).unwrap());

        let zetels = DhondtZetelVerdeler::zetel_verdeling(nb_zetels, 20., &resultaten);
        assert_eq!(16, *zetels.get(&Partij::Nva).unwrap());
        assert_eq!(9, *zetels.get(&Partij::OpenVld).unwrap());
        assert_eq!(0, *zetels.get(&Partij::Vooruit).unwrap());
    }
}
