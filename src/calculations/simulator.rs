use crate::prelude::*;

#[derive(Debug)]
pub(crate) struct PeilingSimulator<'a> {
    peiling: Peiling<'a>,
    pub(crate) simulaties: HashMap<(KieskringNaam, Partij), Vec<u32>>,
    pub(crate) simulatie_totalen: HashMap<Partij, Vec<u32>>,
    pub(crate) partijen: HashSet<Partij>,
    pub(crate) kieskringen: HashSet<KieskringNaam>,
}

impl<'a> PeilingSimulator<'a> {
    pub(crate) fn from_peiling(peiling: Peiling<'a>) -> Self {
        Self {
            peiling,
            simulaties: HashMap::new(),
            simulatie_totalen: HashMap::new(),
            partijen: HashSet::new(),
            kieskringen: HashSet::new(),
        }
    }

    pub(crate) fn run(&mut self, nb_simulaties: usize) {
        let sample = (0..nb_simulaties)
            .into_par_iter()
            .map(|_| self.peiling.simulate_zetelverdeling_per_kieskring())
            .collect::<Vec<_>>();
        for simulatie_resultaat in sample {
            for (kieskring, stem_resultaat) in simulatie_resultaat {
                for (partij, zetels) in stem_resultaat {
                    self.simulaties
                        .entry((kieskring, partij))
                        .or_insert_with(Vec::new)
                        .push(zetels);
                    self.partijen.insert(partij);
                    self.kieskringen.insert(kieskring);
                }
            }
        }
        for partij in self.partijen.iter() {
            let totaal =
                self.kieskringen
                    .iter()
                    .fold(vec![0; nb_simulaties], |mut acc, kieskring| {
                        acc.iter_mut()
                            .zip(self.simulaties[&(*kieskring, *partij)].iter())
                            .for_each(|(acc_e, el)| *acc_e += el);
                        acc
                    });
            self.simulatie_totalen.insert(*partij, totaal);
        }
    }

    pub(crate) fn nb_simulaties(&self) -> usize {
        self.simulaties
            .values()
            .next()
            .map(|list| list.len())
            .unwrap_or(0)
    }

    pub(crate) fn zetels_report(&self) {
        let mut partijen = self.partijen.iter().copied().collect::<Vec<Partij>>();
        let mut kieskringen = self
            .kieskringen
            .iter()
            .copied()
            .collect::<Vec<KieskringNaam>>();
        partijen.sort_unstable();
        kieskringen.sort_unstable();

        let title = once(String::from("Partij"))
            .chain(kieskringen.iter().map(|k| k.to_string()))
            .chain(once(String::from("Totaal")))
            .map(|c| c.cell())
            .collect::<Vec<_>>();
        let rows = partijen
            .iter()
            .map(|partij| {
                once(partij.to_string())
                    .chain(kieskringen.iter().map(|kieskring| {
                        format!("{:?}", stats(&self.simulaties[&(*kieskring, *partij)]))
                    }))
                    .chain(once(format!(
                        "{:?}",
                        stats(&self.simulatie_totalen[partij])
                    )))
                    .map(|c| c.cell())
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        let table = rows.table().title(title).bold(true);
        println!();
        print_stdout(table).expect("Resultaten moeten in een tabel geprint kunnen worden.");
        println!();
    }

    pub(crate) fn coalitie_report(&self) {
        let meerderheids_zetels = self.peiling.assemblee.totale_zetels() / 2 + 1;
        let nb_simulaties = self.nb_simulaties() as f32;
        let title = vec![
            "Coalitie".cell(),
            "(min / 5% / mediaan / 95% / max)".cell(),
            "% op meerderheid".cell(),
        ];
        let rows = COALITIES
            .iter()
            .map(|coalitie| {
                let zetel_som =
                    coalitie
                        .iter()
                        .fold(vec![0u32; self.nb_simulaties()], |mut acc, partij| {
                            acc.iter_mut()
                                .zip(self.simulatie_totalen[&partij].clone())
                                .for_each(|(acc_e, el)| *acc_e += el);
                            acc
                        });
                let summary = stats(&zetel_som);
                let meerderheid_kans = zetel_som
                    .iter()
                    .filter(|e| **e >= meerderheids_zetels)
                    .count() as f32
                    / nb_simulaties
                    * 100.;
                vec![
                    coalitie.naam.clone().cell(),
                    format!("{:?}", summary).cell(),
                    format!("{:.02}%", meerderheid_kans).cell(),
                ]
            })
            .collect::<Vec<_>>();

        let table = rows.table().title(title).bold(true);
        println!();
        print_stdout(table).expect("Resultaten moeten in een tabel geprint kunnen worden.");
        println!();
    }

    pub(crate) fn coalitie_report_extended(&self) {
        let meerderheids_zetels = self.peiling.assemblee.totale_zetels() / 2 + 1;
        let title = vec![
            "Coalitie".cell(),
            "(min / 5% / mediaan / 95% / max)".cell(),
            format!("% op {} zetels", meerderheids_zetels - 1).cell(),
            format!("% op {} zetels", meerderheids_zetels).cell(),
            format!("% op {} zetels", meerderheids_zetels + 1).cell(),
            format!("% op {} zetels", meerderheids_zetels + 2).cell(),
            format!("% op {} zetels", meerderheids_zetels + 3).cell(),
        ];
        let rows = COALITIES
            .iter()
            .map(|coalitie| {
                let zetel_som =
                    coalitie
                        .iter()
                        .fold(vec![0u32; self.nb_simulaties()], |mut acc, partij| {
                            acc.iter_mut()
                                .zip(self.simulatie_totalen[&partij].clone())
                                .for_each(|(acc_e, el)| *acc_e += el);
                            acc
                        });
                let summary = stats(&zetel_som);
                vec![
                    coalitie.naam.clone().cell(),
                    format!("{:?}", summary).cell(),
                    format!(
                        "{:.02}%",
                        perc_exceeding(&zetel_som, meerderheids_zetels - 1)
                    )
                    .cell(),
                    format!("{:.02}%", perc_exceeding(&zetel_som, meerderheids_zetels)).cell(),
                    format!(
                        "{:.02}%",
                        perc_exceeding(&zetel_som, meerderheids_zetels + 1)
                    )
                    .cell(),
                    format!(
                        "{:.02}%",
                        perc_exceeding(&zetel_som, meerderheids_zetels + 2)
                    )
                    .cell(),
                    format!(
                        "{:.02}%",
                        perc_exceeding(&zetel_som, meerderheids_zetels + 3)
                    )
                    .cell(),
                ]
            })
            .collect::<Vec<_>>();

        let table = rows.table().title(title).bold(true);
        println!();
        print_stdout(table).expect("Resultaten moeten in een tabel geprint kunnen worden.");
        println!();
    }
}
